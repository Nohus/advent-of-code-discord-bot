#!/bin/bash

function runBot() {
    ./gradlew shadowjar
    rm app.conf
    cp ../app.conf app.conf
    java -jar build/libs/aocbot.jar &
}

git clone https://gitlab.com/Nohus/advent-of-code-discord-bot.git
cd advent-of-code-discord-bot || exit
runBot
while true; do
    current=$(git pull | grep -c "up to date")
    if [[ ${current} -eq 0 ]]; then
        killall java
        runBot
    fi
    sleep 60s
done
