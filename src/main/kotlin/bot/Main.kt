package bot

import java.time.Duration
import java.time.Instant
import java.util.Calendar
import java.util.GregorianCalendar
import java.util.TimeZone

/**
 * Created by nohus on 03/12/18.
 */

val bot = BotGroup(getBots())
val aoc = Aoc(Settings.year, Settings.leaderboard)
val list = mutableListOf<Member>()

fun main() {
    var lastUpdate = Instant.EPOCH
    while (true) {
        val sinceLastUpdate = Duration.between(lastUpdate, Instant.now())
        if (sinceLastUpdate >= getCurrentFrequency()) {
            lastUpdate = Instant.now()
            update()
        }
        bot.setNextPuzzleStatus(TimeFormatting.formatShort(PuzzleTime.getTimeToNextPuzzle()))
//        bot.setStatus("out for next year!")
        Thread.sleep(5000)
    }
}

private fun getBots(): List<Bot> {
    val bots = mutableListOf<Bot>()
    if (Settings.Discord.channels.isNotEmpty()) bots += DiscordBot()
    if (Settings.Slack.webhook.isNotBlank()) bots += SlackBot()
    return bots
}

fun getCurrentFrequency(): Duration {
    val calendar = GregorianCalendar(TimeZone.getTimeZone("UTC"))
    return if (calendar[Calendar.HOUR_OF_DAY] == 5) { // First hour
        when {
            calendar[Calendar.MINUTE] < 10 -> Duration.ofMinutes(1) // First 10 minutes
            else -> Duration.ofMinutes(2) // Later in the first hour
        }
    } else Duration.ofMinutes(5) // Later in day
}

fun update() {
    println("Requesting new leaderboard data")
    getMemberList(aoc.getLeaderboard())?.let { newList ->
        LeaderboardChecker.getLeaderboardUpdates(list, newList).forEach {
            bot.sendMessage(it)
        }
        list.clear()
        list.addAll(newList)
    } ?: println("Member list was null")
}

fun getMemberList(leaderboard: Leaderboard?): List<Member>? {
    return leaderboard?.members?.values?.sortedWith(
        compareBy(
            { it.localScore },
            { -it.id.toInt() }
        )
    )?.reversed()
}
