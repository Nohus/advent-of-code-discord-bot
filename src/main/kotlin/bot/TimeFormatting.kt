package bot

import java.time.Duration
import kotlin.math.abs

object TimeFormatting {

    fun format(time: Duration): String {
        val days = time.toDays().toInt()
        val hours = time.toHours().toInt()
        val minutes = time.toMinutesPart()
        val seconds = time.toSecondsPart()
        return when {
            days >= 2 -> formatDays(days)
            hours >= 24 -> formatHours(hours)
            hours > 0 -> "${formatHours(hours)} and ${formatMinutes(minutes)}"
            minutes >= 10 -> formatMinutes(minutes)
            minutes > 0 -> "${formatMinutes(minutes)} and ${formatSeconds(seconds)}"
            else -> formatSeconds(seconds)
        }
    }

    fun formatShort(time: Duration): String {
        val hours = time.toHours().toInt()
        val minutes = time.toMinutesPart()
        return when {
            hours > 0 -> formatHours(hours)
            minutes > 0 -> formatMinutes(minutes)
            else -> "mere moments!"
        }
    }

    private fun formatDays(amount: Int) = formatNoun("a day", "days", amount)
    private fun formatHours(amount: Int) = formatNoun("an hour", "hours", amount)
    private fun formatMinutes(amount: Int) = formatNoun("a minute", "minutes", amount)
    private fun formatSeconds(amount: Int) = formatNoun("a second", "seconds", amount)

    private fun formatNoun(singular: String, plural: String, amount: Int): String {
        return if (abs(amount) != 1) "$amount $plural" else singular
    }
}
