package bot

import io.ktor.client.HttpClient
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import kotlinx.coroutines.runBlocking

/**
 * Created by Marcin Wisniowski (Nohus) on 02/12/2019.
 */

class SlackBot : Bot() {

    private val client = HttpClient()

    override fun sendMessage(text: String) {
        val webhook = Settings.Slack.webhook
        val formatted = text.replace("**", "*")
        println("Sending message to Slack Webhook: $text")
        val body = "{\"text\":\"$formatted\"}"
        runBlocking {
            client.post<String> {
                url(webhook)
                this.body = TextContent(body, ContentType.Application.Json)
            }
        }
    }

    override fun setNextPuzzleStatus(time: String) {
        // No status on Slack
    }

    override fun setStatus(text: String) {
        // No status on Slack
    }
}
