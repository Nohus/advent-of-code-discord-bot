package bot

import com.squareup.moshi.Json

/**
 * Created by nohus on 03/12/18.
 */

class Leaderboard(
    val members: Map<Int, Member>
)

data class Member(
    @Json(name = "local_score") val localScore: Int,
    @Json(name = "completion_day_level") val completionDayLevel: Map<Int, Map<Int, Completion>>,
    @Json(name = "last_star_ts") val lastStarTimestamp: Long,
    @Json(name = "global_score") val globalScore: Int,
    val name: String?,
    val stars: Int,
    val id: String
) {
    override fun toString(): String {
        return "$displayName: $stars*, score: $localScore"
    }

    val displayName: String get() {
        return name ?: "Anonymous $id"
    }
}

data class Completion(
    @Json(name = "get_star_ts") val timestamp: Long
)
