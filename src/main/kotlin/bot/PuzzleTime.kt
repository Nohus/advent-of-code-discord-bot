package bot

import java.time.Duration
import java.time.Instant
import java.util.Calendar
import java.util.GregorianCalendar
import java.util.TimeZone

object PuzzleTime {

    fun getReleaseTime(day: Int): Instant {
        val calendar = GregorianCalendar(TimeZone.getTimeZone("UTC")).apply {
            set(Calendar.MONTH, Calendar.DECEMBER)
            set(Calendar.DAY_OF_MONTH, day)
            set(Calendar.HOUR_OF_DAY, 5)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        return calendar.toInstant()
    }

    fun getTimeToNextPuzzle(): Duration {
        val calendar = GregorianCalendar(TimeZone.getTimeZone("UTC")).apply {
            if (get(Calendar.MONTH) == Calendar.DECEMBER) {
                if (get(Calendar.HOUR_OF_DAY) >= 5) {
                    add(Calendar.DAY_OF_MONTH, 1)
                }
            } else {
                set(Calendar.MONTH, Calendar.DECEMBER)
                set(Calendar.DAY_OF_MONTH, 1)
            }
            set(Calendar.HOUR_OF_DAY, 5)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        return Duration.between(Instant.now(), calendar.toInstant())
    }
}
