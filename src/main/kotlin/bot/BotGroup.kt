package bot

/**
 * Created by Marcin Wisniowski (Nohus) on 02/12/2019.
 */

class BotGroup(private val bots: List<Bot>) : Bot() {

    override fun sendMessage(text: String) {
        bots.forEach {
            it.sendMessage(text)
        }
    }

    override fun setNextPuzzleStatus(time: String) {
        bots.forEach {
            it.setNextPuzzleStatus(time)
        }
    }

    override fun setStatus(text: String) {
        bots.forEach {
            it.setStatus(text)
        }
    }
}
