package bot

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Path

/**
 * Created by nohus on 03/12/18.
 */

interface AocService {
    @Headers("User-Agent: Advent of Code Discord/Slack bot (email: nohus@nohus.eu)")
    @GET("{year}/leaderboard/private/view/{leaderboard}.json")
    fun getLeaderboard(@Header("cookie") cookie: String, @Path("year") year: String, @Path("leaderboard") leaderboard: String): Call<Leaderboard>
}
