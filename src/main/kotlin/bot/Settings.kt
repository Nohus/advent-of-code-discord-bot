package bot

import dev.nohus.autokonfig.types.Group
import dev.nohus.autokonfig.types.StringSetting
import dev.nohus.autokonfig.types.StringSettingType

/**
 * Created by nohus on 01/12/18.
 */

object Settings {
    val session by StringSetting()
    val year by StringSetting()
    val leaderboard by StringSetting()
    object Discord : Group() {
        val botToken by StringSetting()
        val channels by ListSetting(StringSettingType)
    }
    object Slack : Group() {
        val webhook by StringSetting()
    }
}
