package bot

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by nohus on 03/12/18.
 */

class Aoc(private val year: String, private val leaderboard: String) {

    companion object {
        private const val BASE_URL = "https://adventofcode.com/"
    }

    private val service: AocService
    private val cookie: String

    init {
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
        service = retrofit.create(AocService::class.java)
        val session = Settings.session
        cookie = "session=$session"
    }

    fun getLeaderboard(): Leaderboard? {
        return try {
            service.getLeaderboard(cookie, year, leaderboard).execute().body()
        } catch (e: Exception) {
            null
        }
    }
}
