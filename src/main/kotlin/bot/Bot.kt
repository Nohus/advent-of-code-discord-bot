package bot

/**
 * Created by Marcin Wisniowski (Nohus) on 02/12/2019.
 */

abstract class Bot {
    abstract fun sendMessage(text: String)
    abstract fun setNextPuzzleStatus(time: String)
    abstract fun setStatus(text: String)
}
