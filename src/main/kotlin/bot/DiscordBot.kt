package bot

import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.OnlineStatus
import net.dv8tion.jda.api.entities.Activity

/**
 * Created by nohus on 03/12/18.
 */

class DiscordBot : Bot() {

    private val bot: JDA

    init {
        val botToken = Settings.Discord.botToken
        bot = JDABuilder.createDefault(botToken).build()
        bot.awaitReady()
    }

    override fun sendMessage(text: String) {
        bot.textChannels.forEach {
            if (it.name in Settings.Discord.channels) {
                if (it.canTalk()) {
                    println("Sending message in channel ${it.name}: $text")
                    it.sendMessage(text).complete()
                } else {
                    println("No permission to talk in channel: ${it.name}!")
                }
            }
        }
    }

    override fun setNextPuzzleStatus(time: String) {
        setStatus("new puzzle in $time")
    }

    override fun setStatus(text: String) {
        val activity = Activity.watching(text)
        bot.presence.setPresence(OnlineStatus.ONLINE, activity)
    }
}
