package bot

import java.time.Duration
import java.time.Instant

object LeaderboardChecker {

    fun getLeaderboardUpdates(old: List<Member>, new: List<Member>): List<String> {
        if (old.isEmpty()) {
            println("First leaderboard")
            return emptyList()
        }
        if (old.toString() == new.toString()) {
            println("No updates")
            return emptyList()
        } else {
            println("Old: $old")
            println("New: $new")
        }

        val updates = mutableListOf<String>()
        updates += getMembershipUpdates(old, new)
        updates += getMemberUpdates(old, new)
        return updates
    }

    private fun getMembershipUpdates(old: List<Member>, new: List<Member>): List<String> {
        val updates = mutableListOf<String>()
        val newMembers = new.filter { it.displayName !in old.map { it.displayName } }
        val missingMembers = old.filter { it.displayName !in new.map { it.displayName } }
        updates += missingMembers.map {
            ":door: **${it.displayName}** left the leaderboard."
        }
        updates += newMembers.map { member ->
            val rank = new.indexOfFirst { it.displayName == member.displayName } + 1
            ":wave: **${member.displayName}** joined the leaderboard and is at rank ${numberToEmoji(rank)} now."
        }
        return updates
    }

    private fun getMemberUpdates(old: List<Member>, new: List<Member>): List<String> {
        val updates = mutableListOf<String>()
        val members = new.filter { it.displayName in old.map { it.displayName } }
        members.forEach { member ->
            val previousEntry = old.first { it.displayName == member.displayName }
            if (member.stars > previousEntry.stars) {
                println("${member.displayName} gained stars")
                for (day in 1..25) {
                    for (part in 1..2) {
                        val had = previousEntry.completionDayLevel[day]?.get(part) != null
                        val has = member.completionDayLevel[day]?.get(part) != null
                        if (has && !had) {
                            // Find completion time
                            val startTimestamp = PuzzleTime.getReleaseTime(day)
                            val completionTimestamp = member.completionDayLevel[day]?.get(part)?.timestamp?.let { Instant.ofEpochSecond(it) } ?: break
                            val timing = TimeFormatting.format(Duration.between(startTimestamp, completionTimestamp))
                            // Find relative time
                            var previous: Member? = null
                            var previousTimestamp = Instant.EPOCH
                            members.filter { it.displayName != member.displayName }.forEach {
                                it.completionDayLevel[day]?.get(part)?.timestamp?.let { Instant.ofEpochSecond(it) }?.let { timestamp ->
                                    if (timestamp > previousTimestamp && timestamp <= completionTimestamp) {
                                        previous = it
                                        previousTimestamp = timestamp
                                    }
                                }
                            }
                            // Message
                            val emoji = if (part == 1) ":star:" else ":star2:"
                            updates += if (previous != null) {
                                val time = TimeFormatting.format(Duration.between(previousTimestamp, completionTimestamp))
                                "$emoji **${member.displayName}** completed part $part of day $day in `$timing`, `$time` after **${previous?.displayName}**."
                            } else {
                                "$emoji **${member.displayName}** was the first to complete part $part of day $day, in `$timing`!"
                            }
                        }
                    }
                }
            }
            val oldRank = old.indexOfFirst { it.displayName == member.displayName } + 1
            val newRank = new.indexOfFirst { it.displayName == member.displayName } + 1
            if (newRank < oldRank) {
                if (member.localScore != previousEntry.localScore || member.stars != previousEntry.stars) {
                    // Find who was overtaken with this advancement
                    val overtaken = old.filterIndexed { index, _ -> (index + 1) in newRank until oldRank }
                        .map { "**${it.displayName}**" }
                    val overtakenText = if (overtaken.size <= 8) {
                        overtaken.joinToString(", ", " and ")
                    } else {
                        "${numberToEmoji(overtaken.size)} other participants"
                    }
                    updates += ":chart_with_upwards_trend: **${member.displayName}** advanced from rank ${numberToEmoji(oldRank)} to rank ${numberToEmoji(newRank)}, overtaking $overtakenText!"
                }
            }
        }
        return updates
    }

    private fun numberToEmoji(number: Int): String {
        return number.toString().map {
            when (it) {
                '0' -> ":zero:"
                '1' -> ":one:"
                '2' -> ":two:"
                '3' -> ":three:"
                '4' -> ":four:"
                '5' -> ":five:"
                '6' -> ":six:"
                '7' -> ":seven:"
                '8' -> ":eight:"
                '9' -> ":nine:"
                else -> "$it"
            }
        }.joinToString("")
    }

    private fun List<String>.joinToString(separator: String, lastSeparator: String): String {
        return if (size > 1) {
            dropLast(1).joinToString(separator) + lastSeparator + last()
        } else {
            singleOrNull() ?: ""
        }
    }
}
