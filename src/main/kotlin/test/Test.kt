package test

import bot.Leaderboard
import bot.LeaderboardChecker
import bot.getMemberList
import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import kotlin.system.exitProcess

/**
 * Created by nohus on 03/12/18.
 */

val testLeaderboardString =
    """
    {
  "owner_id": "216440",
  "event": "2019",
  "members": {
    "53883": {
      "local_score": 57,
      "last_star_ts": "1605805334",
      "id": "53883",
      "completion_day_level": {
        "1": {
          "1": {
            "get_star_ts": "1605805334"
          },
          "2": {
            "get_star_ts": "1605805334"
          }
        },
        "2": {
          "1": {
            "get_star_ts": "1605805334"
          }
        }
      },
      "name": "James C.",
      "stars": 3,
      "global_score": 0
    },
    "191186": {
      "name": "AC-Aksan",
      "completion_day_level": {
        "1": {
          "1": {
            "get_star_ts": "1605805334"
          },
          "2": {
            "get_star_ts": "1605805334"
          }
        }
      },
      "id": "191186",
      "local_score": 48,
      "last_star_ts": "1605805334",
      "global_score": 0,
      "stars": 2
    }
  }
}"""

val testLeaderboardString2 =
    """
    {
  "owner_id": "216440",
  "event": "2019",
  "members": {
    "53883": {
      "local_score": 57,
      "last_star_ts": "1605805334",
      "id": "53883",
      "completion_day_level": {
        "1": {
          "1": {
            "get_star_ts": "1605805334"
          },
          "2": {
            "get_star_ts": "1605805334"
          }
        },
        "2": {
          "1": {
            "get_star_ts": "1605805334"
          }
        }
      },
      "name": "James C.",
      "stars": 3,
      "global_score": 0
    },
    "191186": {
      "name": "AC-Aksan",
      "completion_day_level": {
        "1": {
          "1": {
            "get_star_ts": "1605805334"
          },
          "2": {
            "get_star_ts": "1605805334"
          }
        },
        "2": {
          "1": {
            "get_star_ts": "1605807334"
          },
          "2": {
            "get_star_ts": "1605830334"
          }
        }
      },
      "id": "191186",
      "local_score": 60,
      "last_star_ts": "1605805334",
      "global_score": 0,
      "stars": 4
    }
  }
}"""

fun getLeaderboardFromString(text: String): Leaderboard {
    val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()
    val adapter = moshi.adapter(Leaderboard::class.java)
    return adapter.fromJson(text)!!
}

fun main() {
    val listA = getMemberList(getLeaderboardFromString(testLeaderboardString))!!.toMutableList()
    val listB = getMemberList(getLeaderboardFromString(testLeaderboardString2))!!
    LeaderboardChecker.getLeaderboardUpdates(listA, listB).forEach {
        println(it)
    }
    exitProcess(0)
}
