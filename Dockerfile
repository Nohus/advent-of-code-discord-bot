FROM openjdk:11-buster
RUN apt-get update && apt-get -y install git psmisc
RUN mkdir /bot
COPY "init.sh" /bot
COPY "app.conf" /bot
WORKDIR /bot
CMD ["./init.sh"]
